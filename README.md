# Presentation Slides used in previous FOSS NSS meetups and weekly sessions are listed here

### Currently pushed slides

* Peermeet 20 - Date: 16-01-2020
* Version Control using Git - Date: 28-01-2020
* Basic Linux Commands - Date: 04-02-2020
* Increasing productivity using emacs - Mini-conf FOSSNSS - Date: 13-10-2020
* Introduction to WikiData - Mini-conf FOSSNSS - Date: 13-10-2020
* Open Street Maps, Importance, Intro to Tools - Date: 13-10-2020
* Gaming on GNU/Linux - Date: 13-10-2020